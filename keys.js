const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

const gettingKeys = (obj) => {
  if (!obj) return [];
  let keysArr = [];
  for (let each in obj) {
    keysArr.push(each);
  }

  return keysArr;
};

module.exports = gettingKeys;


