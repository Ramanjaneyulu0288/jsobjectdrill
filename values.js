const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

const gettingValues = (obj) => {
  if (!obj) return [];
  let valuesArr = [];
  for (let each in obj) {
    valuesArr.push(obj[each]);
  }

  return valuesArr;
};

module.exports = gettingValues;